class Payment:
    def __init__(self):
        self.dict = {}

    def create(self, code, name):

        if code in self.dict:
            return {'Is Error': True, 'Error Message': "Payment code '{}' already exists. Cannot Create. ".format(code)}
        else:
            self.dict[code] = {"Name" : name}
        return {'Is Error': False, 'Error Message': ""}
    
    def read(self, code):

        if code in self.dict:
            retPayment = self.dict[code]
        else:
            return ({'Is Error': True, 'Error Message': "Payment Code '{}' not found. Cannot Read.".format(code)},{})

        return ({'Is Error': False, 'Error Message': ""},retPayment)
    
    def update(self, code, newName, newUnits):

        if code in self.dict:
            self.dict[code]["Name"] = newName

        else:
            return {'Is Error': True, 'Error Message': "Product Code '{}' not found. Cannot Update.".format(code)}

        return {'Is Error': False, 'Error Message': ""}
    
    def delete(self, code):

        if code in self.dict:
            del self.dict[code]

        else:
            return {'Is Error': True, 'Error Message': "Payment Code '{}' not found. Cannot Delete".format(code)}
        return {'Is Error': False, 'Error Message': ""}

    def dump(self):
        # Will dump all payment data by returning 1 dictionary as output.
        return (self.dict)