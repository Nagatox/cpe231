from helper_functions import *
from Product import *
from Customer import *

class Receipt:
    def __init__(self):
        self.dict = {}
    
    def __updateLineItem (self, receiptLineTuplesList):
        total = 0
        receiptLineList = []
        for lineItem in receiptLineTuplesList:
            receiptLineItem = {}
            receiptLineItem["Invoice No"] = lineItem["Invoice No"]
            receiptLineItem["Amount Paid Here"] = lineItem["Amount Paid Here"]
            total = total + receiptLineItem["Amount Paid Here"]
            receiptLineList.append(receiptLineItem)
            
        return receiptLineList, total

    def create(self, receiptNo, receiptDate, customerCode, paymentCode, paymentReference, remarks, receiptLineTuplesList):

        if receiptNo in self.dict:
            return {'Is Error': True, 'Error Message': "Receipt No '{}' already exists. Cannot Create. ".format(receiptNo)}
        else:
            receiptLineItemList, total = self.__updateLineItem(receiptLineTuplesList)
            
            self.dict[invoiceNo] = {"Date" : receiptDate,"Customer Code" : customerCode,"Payment Code" : paymentCode,"Payment Reference" : paymentReference, "Remarks": remarks, "Total" : total,"Items List" : invoiceLineItemList}
        return {'Is Error': False, 'Error Message': ""}

    def read(self, receiptNo):

        if receiptNo in self.dict:
            retRecept = self.dict[receiptNo]
        else:
            return ({'Is Error': True, 'Error Message': "Receipt No '{}' not found. Cannot Read.".format(receiptNo)},{})

        return ({'Is Error': False, 'Error Message': ""},retRecept)

    def update(self, receiptNo, newReceiptDate, newCustomerCode, newPaymentCode, newPaymentReference, newRemarks, newReceiptLineTuplesList):

        if receiptNo in self.dict:
            self.dict[receiptNo]["Date"] = newReceiptDate
            self.dict[receiptNo]["Customer Code"] = newCustomerCode
            self.dict[receiptNo]["Payment Code"] = newPaymentCode
            self.dict[receiptNo]["Payment Reference"] = newPaymentReference
            self.dict[receiptNo]["Remarks"] = newRemarks

            receiptLineItemList, total = self.__updateLineItem(newReceiptLineTuplesList)
            
            self.dict[receiptNo]["Total"] = total
            self.dict[receiptNo]["Items List"] = receiptLineItemList
        else:
            return {'Is Error': True, 'Error Message': "Receipt No '{}' not found. Cannot Update.".format(receiptNo)}

        return {'Is Error': False, 'Error Message': ""}

    def delete(self, receiptNo):

        if receiptNo in self.dict:
            del self.dict[receiptNo]

        else:
            return {'Is Error': True, 'Error Message': "Receipt No '{}' not found. Cannot Delete".format(receiptNo)}
        return {'Is Error': False, 'Error Message': ""}

    def dump(self):
        # Will dump all products data by returning 1 dictionary as output.
        return (self.dict)

    def update_invoice_line(self, receiptNo, invoiceNo, amountPaidHere):

        if receiptNo in self.dict:
            total = 0
            newReceiptLineTuplesList = []
            bUpdated = False
            for lineItem in self.dict[receiptNo]["Items List"]:
                receiptLineItem = {}
                if lineItem["Invoice No"] == invoiceNo:
                    receiptLineItem["Amount Paid Here"] = amountPaidHere

                    newReceiptLineTuplesList.append(receiptLineItem)
                    bUpdated = True
                else:
                    newReceiptLineTuplesList.append(lineItem)
            
            if bUpdated:
                receiptLineItemList, total = self.__updateLineItem(newReceiptLineTuplesList)

                self.dict[receiptNo]["Total"] = total
                self.dict[receiptNo]["Items List"] = receiptLineItemList
            else:
                return {'Is Error': True, 'Error Message': "Invoice Code '{}' not found in Receipt No '{}'. Cannot Update.".format(invoiceNo, receiptNo)}
        else:
            return {'Is Error': True, 'Error Message': "Receipt No '{}' not found. Cannot Update.".format(receiptNo)}

        return {'Is Error': False, 'Error Message': ""}

    def delete_invoice_line(self, receiptNo, invoiceNo):

        if receiptNo in self.dict:
            total = 0
            receiptLineItemList = []
            bDeleted = False
            for lineItem in self.dict[receiptNo]["Items List"]:
                invoiceLineItem = {}
                if lineItem["Invoice No"] == invoiceNo:
                    bDeleted = True
                else:
                    receiptLineItemList.append(lineItem)
            
            if bDeleted:
                receiptLineItemList, total = self.__updateLineItem(receiptLineItemList)

                self.dict[receiptNo]["Total"] = total
                self.dict[receiptNo]["Items List"] = receiptLineItemList
            else:
                return {'Is Error': True, 'Error Message': "Invoice No '{}' not found in Receipt No '{}'. Cannot Delete.".format(invoiceNo, receiptNo)}
        else:
            return {'Is Error': True, 'Error Message': "Receipt No '{}' not found. Cannot Delete.".format(receiptNo)}

        return {'Is Error': False, 'Error Message': ""}

