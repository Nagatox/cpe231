from django.contrib import admin

# Register your models here.
from receipt.models import PaymentMethod
from receipt.models import Receipt
from receipt.models import ReceiptLineItem


admin.site.register(PaymentMethod)
admin.site.register(Receipt)
admin.site.register(ReceiptLineItem)
