from django.db import models
from invoice.models import *

# Create your models here.

class PaymentMethod(models.Model):
    payment_method_code = models.CharField(max_length=10, primary_key=True)
    payment_method_name = models.CharField(max_length=100, null=True)

    def __str__(self):
        return self.payment_method_code

class Receipt(models.Model):
    receipt_no = models.CharField(max_length=10, primary_key=True)
    date = models.DateField(null=True)
    customer_code = models.ForeignKey(Customer, on_delete=models.CASCADE, db_column='customer_code')
    payment_method_code = models.ForeignKey(PaymentMethod, on_delete=models.CASCADE, db_column='payment_method_code')
    payment_reference = models.CharField(max_length=100, null=True, blank=True)
    total_receipt = models.FloatField(null=True, blank=True)
    remarks = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return '{"receipt_no":"%s","date":"%s","customer_code":"%s","customer_name":"%s","payment_method_code":"%s","payment_method_name":"%s","payment_reference":"%s","total_receipt":"%s","remarks":"%s"}'\
            % (self.receipt_no, self.date, self.customer_code, self.customer_code.name, self.payment_method_code, self.payment_method_code.payment_method_name, self.payment_reference, self.total_receipt, self.remarks)

class ReceiptLineItem(models.Model):
    lineitem = models.IntegerField()
    receipt_no = models.ForeignKey(Receipt, on_delete=models.CASCADE, db_column='receipt_no')
    invoice_no = models.ForeignKey(Invoice, on_delete=models.CASCADE, db_column='invoice_no')
    amount_paid_here = models.FloatField(null=True)

    class Meta:
        unique_together = (("lineitem", "receipt_no"),)

    def __str__(self):
        return '{"lineitem":"%s","receipt_no":"%s","invoice_no":"%s","invoice_full_amount":"%s","amount_paid_here":"%s"}'\
            % (self.lineitem, self.receipt_no.receipt_no, self.invoice_no.invoice_no, self.invoice_no.amount_due, self.amount_paid_here)

