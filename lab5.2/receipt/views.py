from django.shortcuts import render
from django.http import HttpResponse

from django.shortcuts import get_object_or_404
from django.views.generic import View
from django.http import JsonResponse
from django import forms
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.forms.models import model_to_dict
from django.db.models import Max
from receipt.models import *
import json

# Create your views here.
def index(request):
    data = {}
    return render(request, 'receipt/index.html', data)

def test(request):
    data = {}
    return render(request, 'receipt/test.html', data)

class PaymentMethodList(View):
    def get(self, request):
        payment_methods = list(PaymentMethod.objects.all().values())
        data = dict()
        data['payment_methods'] = payment_methods
        response = JsonResponse(data)
        response["Access-Control-Allow-Origin"] = "*"
        return response

class ReceiptList(View):
    def get(self, request):
        receipts = Receipt.objects.select_related()

        list_receipt = [] 
        for receipt in receipts:
            dict_receipt = json.loads(str(receipt))
            list_receipt.append(dict_receipt)

        data = dict()
        data['receipts'] = list_receipt
        response = JsonResponse(data)
        response["Access-Control-Allow-Origin"] = "*"
        return response

class ReceiptDetail(View):
    def get(self, request, pk, pk2):
        receipt_no = pk + "/" + pk2
        receipt = get_object_or_404(Receipt, pk=receipt_no)
        receiptlineitem = ReceiptLineItem.objects.select_related().filter(receipt_no=receipt_no)
        #print(receiptlineitem)
        list_lineitem = [] 
        for lineitem in receiptlineitem:
            dict_lineitem = json.loads(str(lineitem))
            #print (lineitem.invoice_no)
            results = ReceiptLineItem.objects.raw(
                "SELECT RIT.id, COALESCE(SUM(amount_paid_here),0) sum_amount_paid, MIN(I.amount_due) amount_due "
                " , MIN(I.amount_due) - COALESCE(SUM(amount_paid_here),0) invoice_amount_remain "
                "FROM receipt_receiptlineitem RIT JOIN invoice_invoice I ON RIT.invoice_no = I.invoice_no "
                "WHERE RIT.invoice_no = '{}'  "
                "GROUP BY RIT.id, RIT.receipt_no " .format(lineitem.invoice_no)
                )
            dict_lineitem['invoice_amount_remain'] = results[0].invoice_amount_remain
            list_lineitem.append(dict_lineitem)

        data = dict()
        data['receipt'] = model_to_dict(receipt)
        data['receiptlineitem'] = list_lineitem

        response = JsonResponse(data)
        response["Access-Control-Allow-Origin"] = "*"
        return response

class ReceiptForm(forms.ModelForm):
    class Meta:
        model = Receipt
        fields = '__all__'

class ReceiptLineItemForm(forms.ModelForm):
    class Meta:
        model = ReceiptLineItem
        fields = '__all__'

@method_decorator(csrf_exempt, name='dispatch')
class ReceiptCreate(View):
    def post(self, request):
        data = dict()
        request.POST = request.POST.copy()
        if Receipt.objects.count() != 0:
            receipt_no_max = Receipt.objects.aggregate(Max('receipt_no'))['receipt_no__max']
            next_receipt_no = receipt_no_max[0:3] + str(int(receipt_no_max[3:7])+1) + "/" + receipt_no_max[8:10]
        else:
            next_receipt_no = "RCT1000/19"
        request.POST['receipt_no'] = next_receipt_no
        request.POST['date'] = reFormatDateMMDDYYYY(request.POST['date'])
        request.POST['total_receipt'] = reFormatNumber(request.POST['total_receipt'])

        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save()

            dict_lineitem = json.loads(request.POST['lineitem'])
            print(dict_lineitem)
            for lineitem in dict_lineitem['lineitem']:
                lineitem['receipt_no'] = next_receipt_no
                lineitem['amount_paid_here'] = reFormatNumber(lineitem['amount_paid_here'])

                formlineitem = ReceiptLineItemForm(lineitem)
                formlineitem.save()

            data['receipt'] = model_to_dict(receipt)
        else:
            data['error'] = 'form not valid!'

        response = JsonResponse(data)
        response["Access-Control-Allow-Origin"] = "*"
        return response

@method_decorator(csrf_exempt, name='dispatch')
class ReceiptUpdate(View):
    def post(self, request):
        receipt_no = request.POST['receipt_no']
        data = dict()
        receipt = Receipt.objects.get(pk=receipt_no)
        request.POST = request.POST.copy()
        request.POST['date'] = reFormatDateMMDDYYYY(request.POST['date'])
        request.POST['total_receipt'] = reFormatNumber(request.POST['total_receipt'])

        form = ReceiptForm(instance=receipt, data=request.POST)
        if form.is_valid():
            receipt = form.save()

            receiptlineitem = ReceiptLineItem.objects.filter(receipt_no=receipt_no).delete()

            dict_lineitem = json.loads(request.POST['lineitem'])
            for lineitem in dict_lineitem['lineitem']:
                lineitem['receipt_no'] = receipt_no
                lineitem['amount_paid_here'] = reFormatNumber(lineitem['amount_paid_here'])

                formlineitem = ReceiptLineItemForm(lineitem)
                formlineitem.save()

            data['receipt'] = model_to_dict(receipt)
        else:
            data['error'] = 'form not valid!'

        response = JsonResponse(data)
        response["Access-Control-Allow-Origin"] = "*"
        return response

@method_decorator(csrf_exempt, name='dispatch')
class ReceiptDelete(View):
    def post(self, request):
        receipt_no = request.POST['receipt_no']
        data = dict()
        receipt = Receipt.objects.get(pk=receipt_no)
        if receipt:
            receipt.delete()
            data['message'] = "Receipt Deleted!"
        else:
            data['message'] = "Error!"

        return JsonResponse(data)

class ReceiptReport(View):
    def get(self, request, pk, pk2):
        receipt_no = pk + "/" + pk2
        receipt = get_object_or_404(Receipt, pk=receipt_no)
        receiptlineitem = ReceiptLineItem.objects.select_related().filter(receipt_no=receipt_no)

        list_lineitem = [] 
        for lineitem in receiptlineitem:
            dict_lineitem = json.loads(str(lineitem))
            list_lineitem.append(dict_lineitem)

        data = dict()
        data['receipt'] = model_to_dict(receipt)
        data['receiptlineitem'] = list_lineitem
        
        return render(request, 'receipt/report.html', data)

def reFormatDateMMDDYYYY(ddmmyyyy):
        if (ddmmyyyy == ''):
            return ''
        return ddmmyyyy[3:5] + "/" + ddmmyyyy[:2] + "/" + ddmmyyyy[6:]

def reFormatNumber(str):
        if (str == ''):
            return ''
        return str.replace(",", "")

