from django.shortcuts import render
from django.http import HttpResponse

from django.shortcuts import get_object_or_404
from django.views.generic import View
from django.http import JsonResponse
from django import forms
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.forms.models import model_to_dict
from django.db.models import Max
from invoice.models import *
import json

# Create your views here.
def index(request):
    data = {}
    return render(request, 'invoice/index.html', data)

class ProductList(View):
    def get(self, request):
        products = list(Product.objects.all().values())
        data = dict()
        data['products'] = products
        response = JsonResponse(data)
        response["Access-Control-Allow-Origin"] = "*"
        return response

class CustomerList(View):
    def get(self, request):
        customers = list(Customer.objects.all().values())
        data = dict()
        data['customers'] = customers
        response = JsonResponse(data)
        response["Access-Control-Allow-Origin"] = "*"
        return response

class CustomerDetail(View):
    def get(self, request, pk):
        customer = get_object_or_404(Customer, pk=pk)
        data = dict()
        data['customers'] = model_to_dict(customer)
        response = JsonResponse(data)
        response["Access-Control-Allow-Origin"] = "*"
        return response

class InvoiceList(View):
    def get(self, request):
        invoices = list(Invoice.objects.order_by('invoice_no').all().values())
        data = dict()
        data['invoices'] = invoices
        response = JsonResponse(data)
        response["Access-Control-Allow-Origin"] = "*"
        return response

class InvoiceDetail(View):
    def get(self, request, pk, pk2):
        invoice_no = pk + "/" + pk2
        invoice = get_object_or_404(Invoice, pk=invoice_no)
        # invoicelineitem = InvoiceLineItem.objects.raw(
        #     "SELECT * "
        #     "FROM invoice_invoicelineitem LIT JOIN invoice_product P ON LIT.product_code = P.code "
        #     "WHERE LIT.invoice_no = '{}'" .format(pk)
        #     )
        invoicelineitem = InvoiceLineItem.objects.select_related('product_code').filter(invoice_no=invoice_no)

        list_lineitem = [] 
        for lineitem in invoicelineitem:
            dict_lineitem = json.loads(str(lineitem))
            dict_lineitem['product_name'] = lineitem.product_code.name
            dict_lineitem['units'] = lineitem.product_code.units
            list_lineitem.append(dict_lineitem)

        data = dict()
        data['invoice'] = model_to_dict(invoice)
        data['invoicelineitem'] = list_lineitem

        response = JsonResponse(data)
        response["Access-Control-Allow-Origin"] = "*"
        return response

class InvoiceForm(forms.ModelForm):
    class Meta:
        model = Invoice
        fields = '__all__'

class LineItemForm(forms.ModelForm):
    class Meta:
        model = InvoiceLineItem
        fields = '__all__'

@method_decorator(csrf_exempt, name='dispatch')
class InvoiceCreate(View):
    def post(self, request):
        data = dict()
        request.POST = request.POST.copy()
        if Invoice.objects.count() != 0:
            invoice_no_max = Invoice.objects.aggregate(Max('invoice_no'))['invoice_no__max']
            next_invoice_no = invoice_no_max[0:3] + str(int(invoice_no_max[3:6])+1) + "/" + invoice_no_max[7:9]
        else:
            next_invoice_no = "INT100/19"
        request.POST['invoice_no'] = next_invoice_no
        request.POST['date'] = reFormatDateMMDDYYYY(request.POST['date'])
        request.POST['due_date'] = reFormatDateMMDDYYYY(request.POST['due_date'])
        request.POST['total'] = reFormatNumber(request.POST['total'])
        request.POST['vat'] = reFormatNumber(request.POST['vat'])
        request.POST['amount_due'] = reFormatNumber(request.POST['amount_due'])

        form = InvoiceForm(request.POST)
        if form.is_valid():
            invoice = form.save()

            dict_lineitem = json.loads(request.POST['lineitem'])
            for lineitem in dict_lineitem['lineitem']:
                lineitem['invoice_no'] = next_invoice_no
                lineitem['unit_price'] = reFormatNumber(lineitem['unit_price'])
                lineitem['quantity'] = reFormatNumber(lineitem['quantity'])
                lineitem['extended_price'] = reFormatNumber(lineitem['extended_price'])

                formlineitem = LineItemForm(lineitem)
                formlineitem.save()

            data['invoice'] = model_to_dict(invoice)
        else:
            data['error'] = 'form not valid!'

        response = JsonResponse(data)
        response["Access-Control-Allow-Origin"] = "*"
        return response

@method_decorator(csrf_exempt, name='dispatch')
class InvoiceUpdate(View):
    def post(self, request, pk, pk2):
        invoice_no = pk + "/" + pk2
        data = dict()
        invoice = Invoice.objects.get(pk=invoice_no)
        request.POST = request.POST.copy()
        request.POST['invoice_no'] = invoice_no
        request.POST['date'] = reFormatDateMMDDYYYY(request.POST['date'])
        request.POST['due_date'] = reFormatDateMMDDYYYY(request.POST['due_date'])
        request.POST['total'] = reFormatNumber(request.POST['total'])
        request.POST['vat'] = reFormatNumber(request.POST['vat'])
        request.POST['amount_due'] = reFormatNumber(request.POST['amount_due'])

        form = InvoiceForm(instance=invoice, data=request.POST)
        if form.is_valid():
            invoice = form.save()

            invoicelineitem = InvoiceLineItem.objects.filter(invoice_no=invoice_no).delete()

            dict_lineitem = json.loads(request.POST['lineitem'])
            for lineitem in dict_lineitem['lineitem']:
                lineitem['invoice_no'] = invoice_no
                lineitem['unit_price'] = reFormatNumber(lineitem['unit_price'])
                lineitem['quantity'] = reFormatNumber(lineitem['quantity'])
                lineitem['extended_price'] = reFormatNumber(lineitem['extended_price'])
                formlineitem = LineItemForm(lineitem)
                formlineitem.save()

            data['invoice'] = model_to_dict(invoice)
        else:
            data['error'] = 'form not valid!'

        response = JsonResponse(data)
        response["Access-Control-Allow-Origin"] = "*"
        return response

@method_decorator(csrf_exempt, name='dispatch')
class InvoiceDelete(View):
    def post(self, request, pk, pk2):
        invoice_no = pk + "/" + pk2
        data = dict()
        invoice = Invoice.objects.get(pk=invoice_no)
        if invoice:
            invoice.delete()
            data['message'] = "Invoice Deleted!"
        else:
            data['message'] = "Error!"

        return JsonResponse(data)

class InvoiceReport(View):
    def get(self, request, pk, pk2):
        invoice_no = pk + "/" + pk2
        invoice = get_object_or_404(Invoice, pk=invoice_no)
        invoicelineitem = InvoiceLineItem.objects.select_related('product_code').filter(invoice_no=invoice_no)

        list_lineitem = [] 
        for lineitem in invoicelineitem:
            dict_lineitem = json.loads(str(lineitem))
            dict_lineitem['product_name'] = lineitem.product_code.name
            dict_lineitem['units'] = lineitem.product_code.units
            list_lineitem.append(dict_lineitem)

        data = dict()
        data['invoice'] = model_to_dict(invoice)
        data['invoicelineitem'] = list_lineitem
        
        return render(request, 'invoice/report.html', data)

def reFormatDateMMDDYYYY(ddmmyyyy):
        if (ddmmyyyy == ''):
            return ''
        return ddmmyyyy[3:5] + "/" + ddmmyyyy[:2] + "/" + ddmmyyyy[6:]

def reFormatNumber(str):
        if (str == ''):
            return ''
        return str.replace(",", "")