
from DBHelper import DBHelper

class ReportProductsSold:
    def Report(self):
        print ("""<html>
                    <head>
                        <title>Report Invoice</title>
                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
                        <script src="cpe.js"></script>
                        <link rel='stylesheet' href='cpe.css'>
                    </head>
                    <body><H1>Report Invoice.</H1>
                    <ul class="nav">
                        <li><a href="ReportListAllInvoices">Report list all invoices</a></li>
                        <li><a href="ReportProductsSold">Report products sold</a></li>
                        <li><a href="ReportListAllProducts">Report list all products</a></li>
                    </ul>""")

        print ("Report products sold")
        db = DBHelper()
        data, columns = db.fetch ('SELECT p.code as "Code", ili.product_code as "Product Code", p.name as "Product Name" '
                              ' , SUM(ili.quantity) as "Total Quantity Sold", SUM(ili.extended_price) as "Total Value Sold" '
                              ' FROM invoice i JOIN invoice_line_item ili ON i.invoice_no = ili.invoice_no '
                              '   JOIN product p ON ili.product_code = p.code '
                              ' GROUP BY p.code, ili.product_code, p.name ')
        print ("<table>")
        print ("<tr>")
        for col in columns: 
            print ("<th>" + col + "</th>")
        print ("</tr>")
        
        for row in data: 
            print ("<tr>")
            for col in row:
                print ("<td>" + str(col) + "</td>")
            print ("</tr>")
        print ("</table>")
        print ("</body></html>")