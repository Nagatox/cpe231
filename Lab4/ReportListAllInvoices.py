
from DBHelper import DBHelper

class ReportListAllInvoices:
    def Report(self):
        print ("""<html>
                    <head>
                        <title>Report Invoice</title>
                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
                        <script src="cpe.js"></script>
                        <link rel='stylesheet' href='cpe.css'>
                    </head>
                    <body><H1>Report Invoice.</H1>
                    <ul class="nav">
                        <li><a href="ReportListAllInvoices">Report list all invoices</a></li>
                        <li><a href="ReportProductsSold">Report products sold</a></li>
                        <li><a href="ReportListAllProducts">Report list all products</a></li>
                    </ul>""")

        print ("Report list all invoices")
        db = DBHelper()
        data, columns = db.fetch ('SELECT i.invoice_no as "Invoice No", i.date as "Date" '
                                ' , i.customer_code as "Customer Code", c.name as "Customer Name" '
                                ' , i.due_date as "Due Date", i.total as "Total", i.vat as "VAT", i.amount_due as "Amount Due" '
                                ' , ili.product_code as "Product Code", p.name as "Product Name" '
                                ' , ili.quantity as "Quantity", ili.unit_price as "Unit Price", ili.extended_price as "Extended Price" '
                                ' FROM invoice i JOIN customer c ON i.customer_code = c.customer_code '
                                '  JOIN invoice_line_item ili ON i.invoice_no = ili.invoice_no '
                                '  JOIN product p ON ili.product_code = p.code '
                                ' ')
        print ("<table>")
        print ("<tr>")
        for col in columns: 
            print ("<th>" + col + "</th>")
        print ("</tr>")
        
        for row in data: 
            print ("<tr>")
            for col in row:
                print ("<td>" + str(col) + "</td>")
            print ("</tr>")
        print ("</table>")
        print ("</body></html>")