
from DBHelper import DBHelper

class ReportListAllProducts:
    def Report(self):
        print ("""<html>
                    <head>
                        <title>Report Invoice</title>
                        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
                        <link rel='stylesheet' href='//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css'>
                        <script src='//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js'></script>
                        <script src="cpe.js"></script>
                        <link rel='stylesheet' href='cpe.css'>
                    </head>
                    <body><H1>Report Invoice.</H1>
                    <ul class="nav">
                        <li><a href="ReportListAllInvoices">Report list all invoices</a></li>
                        <li><a href="ReportProductsSold">Report products sold</a></li>
                        <li><a href="ReportListAllProducts">Report list all products</a></li>
                    </ul>""")

        print ("Report list all invoices")
        db = DBHelper()
        data, columns = db.fetch ('SELECT code as "Code", name as "Name", units as "Units" FROM product ')
        print ("<table id='maintable' class='display'>")
        print ("<thead><tr>")
        for col in columns: 
            print ("<th>" + col + "</th>")
        print ("</tr></thead><tbody>")
        
        for row in data: 
            print ("<tr>")
            for col in row:
                print ("<td>" + str(col) + "</td>")
            print ("</tr>")
        print ("</tbody></table>")
        print ("</body></html>")